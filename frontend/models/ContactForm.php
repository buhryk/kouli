<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $message;
    public $subject;
    public $time;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'message'], 'required'],
            [['subject', 'time'], 'safe'],
            // email has to be a valid email address
            ['email', 'email'],
            [['phone'], 'udokmeci\yii2PhoneValidator\PhoneValidator','country'=>'ua'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Ваше имя',
            'phone' => 'Ваш телефон',
            'email' => 'Ваше E-mail',
            'time' => 'Удобное время для занятий',
            'message' => 'Ваш вопрос или сообщение...',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
//        pr($this);
        return Yii::$app->mailer->compose(
            ['html' => 'contactForm'],
            ['contact' => $this])
            ->setTo($email)
            ->setFrom([$this->email => $this->name])
            ->setSubject($this->subject)
            ->setTextBody($this->message)
            ->send();
    }
}
