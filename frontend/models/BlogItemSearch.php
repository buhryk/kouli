<?php

namespace frontend\models;

use backend\modules\blog\models\ItemLang;
use backend\modules\blog\models\ItemTag;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\blog\models\Item;

/**
 * BlogItemSearch represents the model behind the search form about `backend\modules\blog\models\Item`.
 */
class BlogItemSearch extends Item
{
    public $category;
    public $tag;
    public $key;

    const PER_PAGE = 2;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'tag', 'key'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $actual = false)
    {
        $query = Item::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => self::PER_PAGE]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('lang');

        if ($this->tag) {
            $query->innerJoin(ItemTag::tableName(), ItemTag::tableName().'.item_id='.Item::tableName().'.id')
                ->andWhere(['in', ItemTag::tableName().'.tag_id', $this->tag]);
        }

        if ($this->category) {
            $query->andWhere(['in', 'category_id', $this->category]);
        }

        if ($this->key) {
            $query->andWhere(['like', ItemLang::tableName().'.title', '%'.trim($this->key).'%', false]);
        }

        $query->andWhere(['<', 'published_at', time()])
            ->andWhere(['=', 'status', Item::ACTIVE_YES]);
        if ($actual) {
            $query->andWhere(['=', 'actual', Item::ACTUAL_YES]);
        } else {
            $query->andWhere(['=', 'actual', Item::ACTUAL_NO]);
        }

        $query->groupBy(Item::tableName().'.id')
            ->orderBy('published_at DESC');

        return $dataProvider;
    }
}
