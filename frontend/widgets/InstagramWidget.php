<?php
/**
 * Created by PhpStorm.
 * User: Serhii_Bugryk
 * Date: 18.01.2018
 * Time: 9:06
 */

namespace frontend\widgets;


use Yii;
use yii\base\Widget;

class InstagramWidget extends Widget
{
    public $token;

    public function init()
    {
        parent::init(); // TODO: Change the autogenerated stub
    }

    public function run()
    {
        $this->registerScripts();
        $this->registerStyle();
//        $media = Yii::$app->cache->get('inst');
//
//        if ($media === false) {
            $media = $this->instagramConnect();
//            Yii::$app->cache->set('inst', $media, 3600*3);
//        }
        if ($media->meta->code == 400){
            return false;
        }


        return $this->render('instagram', [
            'media' => $media->data,
        ]);

    }

    public function instagramConnect(){

        // создаем новое подключение к API Instagram

        $instagram_cnct = curl_init(); // инициализация cURL подключения
        curl_setopt($instagram_cnct, CURLOPT_URL,
            "https://api.instagram.com/v1/users/self/media/recent?access_token=" . $this->token); // подключаемся
        curl_setopt($instagram_cnct, CURLOPT_RETURNTRANSFER, 1); // просим вернуть результат
        curl_setopt($instagram_cnct, CURLOPT_TIMEOUT, 15);
        $media = json_decode(curl_exec($instagram_cnct)); // получаем и декодируем данные из JSON
        curl_close($instagram_cnct); // закрываем соединение

        return $media;
    }

    protected function registerStyle()
    {
        $view = $this->getView();
        $view->registerCss("#outer_container{bottom:0;  height:170px; padding:0 5px;}
                                #thumbScroller{position:relative; overflow:hidden;}
                                #thumbScroller .container{position:relative; left:0;}
                                #thumbScroller .content{float:left;}
                                #thumbScroller .content div{ height:100%; font-family:Verdana, Geneva, sans-serif; font-size:13px;}
                                #thumbScroller img{border:none;}
                                #thumbScroller a{border:3px solid #fff; margin:5px; display:block;}
                                #thumbScroller a:hover{border-color:#fff;}
                                #thumbScroller .container {padding-right: 0; padding-left: 0px;}
            ");
    }

    protected function registerScripts()
    {
        $view = $this->getView();
        $view->registerJs("
        var sliderWidth=$('#thumbScroller').width();
        var itemWidth=$('#thumbScroller .content').width();
    
        $('#thumbScroller .content').each(function (i) {
            totalContent=i*itemWidth;
            $('#thumbScroller .container').css('width',totalContent+itemWidth);
        });
    
        $('#thumbScroller').mousemove(function(e){
            var mouseCoords=(e.pageX - this.offsetLeft);
            var mousePercentY=mouseCoords/sliderWidth;
            var destY=-(((totalContent-(sliderWidth-itemWidth))-sliderWidth)*(mousePercentY));
            var thePosA=mouseCoords-destY;
            var thePosB=destY-mouseCoords;
            if(mouseCoords==destY){
                $('#thumbScroller .container').stop();
            }
            if(mouseCoords>destY){
                $('#thumbScroller .container').css('left',-thePosA);
            }
            if(mouseCoords<destY){
                $('#thumbScroller .container').css('left',thePosB);
            }
        });
    
        var fadeSpeed=300;
    
        ", \yii\web\View::POS_READY);
    }
}