<?php
namespace frontend\widgets;


class Instagram extends \yii\bootstrap\Widget
{
    public $tegs ='car';
    public $login='borys13_test';
    public $token='3615125526.829cc3d.ebbf0bcb150d4d5191c4c99878f006bc';
    public $count=20;

    public function init(){}

    public function run() {

        $cache=\Yii::$app->cache;
        $images=$cache->get('instagram');
        if(!$images) {
            $result = $this->send('https://api.instagram.com/v1/tags/' . $this->tegs . '/media/recent?access_token=' . $this->token . '&count=' . $this->count);
            $list = json_decode($result);

            $images = [];
            if (isset($list->data)) {
                foreach ($list->data as $key => $item) {
                    $images[$key]['text'] = $item->caption->text;
                    $images[$key]['link'] = $item->link;
                    $images[$key]['large'] = $item->images->low_resolution->url;
                    $images[$key]['fullsize'] = $item->images->standard_resolution->url;
                    $images[$key]['small'] = $item->images->thumbnail->url;
                }
            }
            $cache->set('instagram',$images);
        }
        
       return $this->render('instagram',['list'=>$images]);

    }
    public function send($url){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_URL, $url);
            $answer = curl_exec($ch);
            curl_close($ch);
            return $answer;
    }
}