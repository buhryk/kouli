<?php
use yii\helpers\Url;
use frontend\assets\GalleryAsset;
use frontend\components\tool\Image;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */

$this->title = $data['meta_title'];
$this->registerMetaTag(['name'=>'description', 'content'=>$data['meta_description']]);
$this->params['body-class']=$data['colum1'];

?>
<div class="furs container-fluid">
    <div class="owl-carousel">
        <?php if(isset($image_list)):
                $i=0;
                foreach ($image_list as $item):
                    ?>
                    <div class="item">
                        <img src="<?=$item['name'] ?>" alt="" />
                        <?php /* ?><? if($i==0): ?>
                            <div class="banner-logo">
                                <img src="images/banner-logo.png" alt="" />
                            </div>
                        <? endif; ?><?php */ ?>
                    </div>
                    <?
                $i++;
             endforeach;
          endif;
        ?>
    </div>
</div>
<?=$data['content'] ?>
<?php if(count($listBlog)): ?>
    <div class="page-title">
        <h2><?=Yii::t('index', 'Последние записи в блоге') ?></h2>
    </div>
    <div class="posts">
        <div class="row">
            <?php $item = $listBlog[0]; ?>
            <div class="col-sm-12 col-md-6">
                <div class="post-box">
                    <div class="post-box-image">
                        <a href="#">
                            <img src="<?=Image::resize($item['image'],436,224) ?>" alt="">
                        </a>
                    </div><!-- /.post-box-image -->

                    <div class="post-box-content">
                        <h2><a href="<?=Url::to(['/blog/'.$item['url']]) ?>"><?=$item['title'] ?></a></h2>

                        <p>
                            <?=$item['description'] ?>
                        </p>

                        <a href="<?=Url::to(['/blog/'.$item['url']]) ?>" class="post-box-read-more btn-border"><?=Yii::t('index', 'Читать') ?></a>
                    </div><!-- /.post-box-content -->
                </div><!-- /.post-box -->
            </div><!-- /.col-sm-6 -->
            <div class="col-sm-12 col-md-6">
                <?php foreach ($listBlog as $key => $item): ?>
                    <?php if ($key !==0): ?>
                            <div class="post-box post-box-small">
                                <div class="post-box-image">
                                    <a href="#">
                                        <img src="<?=Image::resize($item['image'],436,224) ?>" alt="">
                                    </a>
                                </div><!-- /.post-box-image -->

                                <div class="post-box-content">
                                    <h2><a href="<?=Url::to(['/blog/'.$item['url']]) ?>"><?=StringHelper::truncate($item['title'], 50) ?></a></h2>

                                    <p>
                                        <?=StringHelper::truncate($item['description'], 200) ?>
                                    </p>

                                </div><!-- /.post-box-content -->
                            </div><!-- /.post-box -->
                     <?php endif; ?>
                <?php endforeach; ?>
        </div><!-- /.col-sm-6 -->
        </div>
    </div>
<?php endif; ?>
<?= \frontend\widgets\InstagramWidget::widget() ?>