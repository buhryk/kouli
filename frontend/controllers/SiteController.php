<?php
namespace frontend\controllers;

use backend\components\actions\GetImagesAction;
use backend\components\actions\UploadFileAction;
use backend\models\Email;
use backend\models\Form;
use backend\models\People;
use backend\models\Review;
use backend\modules\core\models\Page;
use backend\modules\setting\models\Setting;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public $enableCsrfValidation = false;


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $about_technique = Page::getPageByAlias('o-metodike');
        $kouli = Page::getPageByAlias('kouli');
        $purpose_wellness_project = Page::getPageByAlias('tsel-ozdorovitelnogo-proekta');
        $what_will_it_give = Page::getPageByAlias('chto-eto-dast');

        $reviews = Review::find()->where(['status' => Review::STATUS_ACTIVE])->orderBy(['updated_at' => SORT_DESC])->all();

        $model = new Review();

        if ($model->load(Yii::$app->request->post())  ) {

            $model->image = UploadedFile::getInstance($model, 'image');

            if ($model->image ) {
                $model->image->saveAs('uploads/reviews/' . $model->image->baseName . '.' . $model->image->extension);
                $model->image ='/uploads/reviews/' . $model->image->baseName . '.' . $model->image->extension;
            }

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Спасибо отзыв отправлено на модерацию');
            }


            return $this->refresh();
        } else {
            return $this->render('index', [
                'about_technique' => $about_technique,
                'kouli' => $kouli,
                'purpose_wellness_project' => $purpose_wellness_project,
                'what_will_it_give' => $what_will_it_give,
                'reviews' => $reviews,
                'model' => $model,
            ]);
        }
    }

    public function actionHistory()
    {
        $page = Page::getPageByAlias('istoriya-kouli');

        //pr($people);

        return $this->render('history', [
            'page' => $page
        ]);
    }

    public function actionTeacher()
    {
        $page = Page::getPageByAlias('prepodavateli');

        //pr($people);

        return $this->render('teacher', [
            'page' => $page
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();

        $page = Page::getPageByAlias('kontakty');
//        pr($model);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Setting::getSettingByAlias('email'))) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
                'page' => $page,
            ]);
        }
    }

    public function actionSignup()
    {
        $model = new ContactForm();

        $page = Page::getPageByAlias('zapisatsya');
//        pr($model);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Setting::getSettingByAlias('email'))) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('signup', [
                'model' => $model,
                'page' => $page,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionAddEmail()
    {
        $model = new Email();
        $model->email = Yii::$app->request->post('email');

        if ( $model->save()) {
            return Yii::t('app', 'Вы успешно подписались на новости');
        }

    }

    public function actionSentForm()
    {
        $model = new Form();
        $model->name = Yii::$app->request->post('name');
        $model->email = Yii::$app->request->post('email');
        $model->subject = Yii::$app->request->post('phone');
        $model->message = Yii::$app->request->post('message');

        if ( $model->save()) {
            Yii::$app->session->setFlash('success', 'Контактна форма відправлена');
        }

    }
}
