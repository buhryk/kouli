<?php

namespace frontend\controllers;

use backend\modules\blog\models\BlogItemLike;
use backend\modules\blog\models\BlogItemRatio;
use backend\modules\news\models\News;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Yii;
use backend\modules\blog\models\Item;
use frontend\models\BlogItemSearch;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use backend\modules\page\models\Page;
use backend\modules\seo\models\Seo;

/**
 * BlogController implements the CRUD actions for Item model.
 */
class BlogController extends Controller
{
    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new BlogItemSearch();
        $actual = $searchModel->search(Yii::$app->request->queryParams, true);
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $query = $this->creationQuery();

        $pagination = new Pagination([
            'defaultPageSize' => 12,
            'totalCount' => $query->count(),
        ]);

        $models = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        $pageNumber = Yii::$app->request->get('page');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'models' => $models,
            'pagination' => $pagination,
            'actual' => ($pageNumber && $pageNumber != 1) ? false : $actual,
        ]);
    }

    /**
     * Displays a single Item model.
     * @param string $slug
     * @return mixed
     */
    public function actionView($slug)
    {
        $model = $this->findModelBySlug($slug);
        $modelNext = $this->findModelNext($model->id);

        $model->views = $model->views + 1;
        $model->update(false);

        $month = array(
            "1"=>"Января",
            "2"=>"Февраля",
            "3"=>"Марта",
            "4"=>"Апреля",
            "5"=>"Май",
            "6"=>"Июня",
            "7"=>"Июля",
            "8"=>"Августа",
            "9"=>"Сентября",
            "10"=>"Октября",
            "11"=>"Ноября",
            "12"=>"Декабря"
        );

//        pr();
        return $this->render('view', [
            'model' => $model,
            'modelNext' => $modelNext,
            'month' => $month[Yii::$app->formatter->asDate($model->date, 'MM')],
            'url' => Url::base(true) . Url::current(),
        ]);
    }

    protected function findModelBySlug($slug)
    {
        $model = Item::find()
            ->where(['alias' => $slug, 'status' => Item::ACTIVE_YES])
            ->andWhere([])
            ->joinWith('lang')
            ->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelNext($id)
    {
        $model = Item::find()
            ->where(['status' => Item::ACTIVE_YES, 'actual' => Item::ACTUAL_NO])
            ->andWhere(['<', 'blog_item.id', $id])
            ->joinWith('lang')
            ->orderBy(['blog_item.id' => SORT_DESC])
            ->one();

        return $model;
    }

    public function actionToggleLike($id)
    {
        if (Yii::$app->user->isGuest) {
            throw new AccessDeniedException('Access denied');
        }

        if (Yii::$app->request->isAjax) {
            $model = Item::findOne(['id' => $id, 'status' => Item::ACTIVE_YES]);

            if (!$model) {
                throw new NotFoundHttpException('Page not found');
            }

            if (Yii::$app->request->post('action') && in_array(Yii::$app->request->post('action'), ['create', 'remove'])) {
                $action = Yii::$app->request->post('action');
                Yii::$app->response->format = Response::FORMAT_JSON;

                if ($action == 'create' && $model->canCreateLike()) {
                    $like = new BlogItemLike();
                    $like->user_id = Yii::$app->user->identity->id;
                    $like->item_id = $model->id;

                    if ($like->save()) {
                        $model->likes = BlogItemLike::find()->where(['item_id' => $model->id])->count();
                        $model->update(false);

                        return [
                            'status' => 'success',
                            'likes' => $model->getLikesCount()
                        ];
                    }
                } elseif ($action == 'remove') {

                    BlogItemLike::deleteAll(['item_id' => $model->id, 'user_id' => Yii::$app->user->identity->id]);
                    $model->likes = BlogItemLike::find()->where(['item_id' => $model->id])->count();
                    $model->update(false);

                    return [
                        'status' => 'success',
                        'likes' => $model->getLikesCount()
                    ];
                }
            }

            return ['status' => 'error'];
        }
    }

    public function actionCreateRatio($id)
    {
        if (Yii::$app->user->isGuest) {
            throw new AccessDeniedException('Access denied');
        }

        if (Yii::$app->request->isAjax) {
            $model = Item::findOne(['id' => $id, 'status' => Item::ACTIVE_YES]);

            if (!$model) {
                throw new NotFoundHttpException('Page not found');
            }

            $value = Yii::$app->request->post('value');
            if ($value >= 1 && $value <= 5 && $model->canCreateRatio()) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                $ratio = new BlogItemRatio();
                $ratio->user_id = Yii::$app->user->identity->id;
                $ratio->item_id = $model->id;
                $ratio->ratio = (int)$value;

                if ($ratio->save()) {
                    $model->ratio = BlogItemRatio::find()->where(['item_id' => $model->id])->average('ratio');
                    $model->update(false);

                    return ['status' => 'success',];
                }
            }

            return ['status' => 'error'];
        }
    }

    protected function creationQuery()
    {
        $query = Item::find();
        $query->andWhere(['status' => Item::ACTIVE_YES]);
        $query->andWhere(['actual' => Item::ACTUAL_NO]);
        $query->orderBy(['id' => SORT_DESC]);

        return $query;
    }
}
