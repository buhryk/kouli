<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Montserrat:300,400,700,900&display=swap',
        'https://fonts.googleapis.com/css?family=Space+Mono:400,700&display=swap',
        'css/style.min.css'
    ];
    public $js = [
//        'http://code.jquery.com/jquery-3.3.1.min.js',
//        'http://code.jquery.com/ui/1.12.1/jquery-ui.js',
        'js/slick.js',
        'js/maskinput.js',
        'js/script.js'

    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
