<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\modules\setting\models\Setting;
use common\widgets\WLang;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
$action = Yii::$app->controller->action->id;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="<?= Yii::$app->language ?>"> <!--<![endif]-->

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body >
<?php $this->beginBody() ?>
<header class="header">
    <div class="container">
        <a href="<?= Url::to(['/site/index']) ?>" class="logo">
            <img src="/images/logo.png" alt="">
        </a>
        <div class="menu">
            <ul>
                <li class="<?= $action == 'index' ? 'active' : ''?>"><a href='<?= Url::to(['/site/index']) ?>'>О проекте</a></li>
                <li class="<?= $action == 'history' ? 'active' : ''?>"><a href='<?= Url::to(['/site/history']) ?>'>История</a></li>
                <li class="<?= $action == 'teacher' ? 'active' : ''?>"><a href='<?= Url::to(['/site/teacher']) ?>'>Преподаватели</a></li>
                <li class="<?= $action == 'blog' ? 'active' : ''?>"><a href="<?= Url::to(['/blog']) ?>">Блог</a></li>
                <li class="<?= $action == 'contact' ? 'active' : ''?>"><a href='<?= Url::to(['/site/contact']) ?>'>Контакты</a></li>
                <?= WLang::widget();?>
            </ul>
        </div>
        <div class="menu-btn">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
</header>

<?php if( Yii::$app->session->hasFlash('success') ): ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php endif;?>



    <!--////////////////////////////////////Container-->
    <?= $content ?>

    <!--////////////////////////////////////Footer-->

<footer class="footer">
    <div class="top-line">
        <div class="container">
            <a href="<?= Url::to(['/site/index']) ?>" class="logo">
                <img src="/images/logo.png" alt="">
            </a>
            <div class="menu">
                <ul>
                    <li class="<?= $action == 'index' ? 'active' : ''?>"><a href='<?= Url::to(['/site/index']) ?>'>О проекте</a></li>
                    <li class="<?= $action == 'history' ? 'active' : ''?>"><a href='<?= Url::to(['/site/history']) ?>'>История</a></li>
                    <li class="<?= $action == 'teacher' ? 'active' : ''?>"><a href='<?= Url::to(['/site/teacher']) ?>'>Преподаватели</a></li>
                    <li class="<?= $action == 'blog' ? 'active' : ''?>"><a href="<?= Url::to(['/blog']) ?>">Блог</a></li>
                    <li class="<?= $action == 'contact' ? 'active' : ''?>"><a href='<?= Url::to(['/site/contact']) ?>'>Контакты</a></li>

                </ul>
            </div>
        </div>
    </div>
    <div class="bottom">
        <div class="container">
            <p>© Copyright 2019</p>
            <div class="tels">
                <?php foreach (explode(',', Setting::getSettingByAlias('phones')) as $phone) { ?>
                    <a href="tel:<?= $phone ?>"><?= $phone ?></a>
                <?php } ?>
            </div>
            <div class="mess">
                <a href="<?= Setting::getSettingByAlias('viber') ?>" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20" fill="none">
                        <path d="M15.1438 2.50219C14.6973 2.07885 12.8905 0.729199 8.86213 0.711107C8.86213 0.711107 4.11313 0.41802 1.80014 2.60351C0.513581 3.92783 0.060123 5.87089 0.0109105 8.27709C-0.038302 10.6833 -0.0980602 15.1918 4.12367 16.4148H4.12719L4.12367 18.2819C4.12367 18.2819 4.09555 19.0381 4.58065 19.1901C5.16417 19.3782 5.50865 18.8029 6.06757 18.1842C6.37339 17.844 6.79521 17.3447 7.11509 16.9648C10.0046 17.2144 12.2226 16.6427 12.4757 16.5595C13.0593 16.3641 16.36 15.9299 16.8943 11.4214C17.4497 6.76824 16.6272 3.83013 15.1438 2.50219ZM15.6324 11.0813C15.1789 14.8444 12.5039 15.0832 12.0117 15.2461C11.8008 15.3148 9.8499 15.8141 7.39982 15.6513C7.39982 15.6513 5.57193 17.92 5.00247 18.5098C4.81616 18.7016 4.61228 18.6835 4.6158 18.3036C4.6158 18.0539 4.62986 15.2026 4.62986 15.2026C4.62634 15.2026 4.62634 15.2026 4.62986 15.2026C1.0514 14.1823 1.26231 10.3432 1.30098 8.33499C1.33965 6.3268 1.70874 4.68045 2.79845 3.57323C4.7564 1.74596 8.78832 2.01733 8.78832 2.01733C12.1945 2.03181 13.8256 3.08837 14.2052 3.44297C15.4601 4.55018 16.0999 7.19882 15.6324 11.0813ZM10.7463 8.15769C10.7603 8.46887 10.3069 8.49058 10.2928 8.1794C10.2541 7.38336 9.89208 6.99619 9.14686 6.95277C8.84456 6.93468 8.87268 6.46792 9.17147 6.48601C10.1522 6.54028 10.6971 7.11922 10.7463 8.15769ZM11.4599 8.56656C11.495 7.03238 10.5635 5.83108 8.79535 5.6972C8.49656 5.67549 8.52819 5.20873 8.82698 5.23044C10.8658 5.38241 11.952 6.82613 11.9133 8.57742C11.9098 8.8886 11.4528 8.87412 11.4599 8.56656ZM13.112 9.05142C13.1155 9.3626 12.6585 9.36622 12.6585 9.05504C12.6374 6.10608 10.7287 4.49953 8.41219 4.48144C8.1134 4.47782 8.1134 4.01467 8.41219 4.01467C11.0029 4.03276 13.0874 5.8745 13.112 9.05142ZM12.7148 12.601V12.6083C12.3351 13.2958 11.6251 14.0556 10.8939 13.8132L10.8869 13.8023C10.1452 13.5888 8.39813 12.6625 7.29436 11.758C6.72491 11.2948 6.20466 10.7484 5.80393 10.2238C5.44186 9.757 5.07629 9.20339 4.72125 8.53762C3.97252 7.14455 3.80731 6.52219 3.80731 6.52219C3.57179 5.76957 4.30646 5.03866 4.97786 4.64788H4.98489C5.30829 4.4742 5.61762 4.53209 5.82502 4.789C5.82502 4.789 6.2609 5.32451 6.44721 5.58865C6.62297 5.8347 6.85848 6.2291 6.98151 6.44982C7.19594 6.84422 7.06236 7.24586 6.85145 7.41231L6.42963 7.75967C6.2152 7.93697 6.24333 8.26624 6.24333 8.26624C6.24333 8.26624 6.86903 10.7014 9.20662 11.3165C9.20662 11.3165 9.5265 11.3455 9.69875 11.1247L10.0362 10.6905C10.1979 10.4734 10.5881 10.3359 10.9712 10.5567C11.488 10.857 12.1453 11.3238 12.5812 11.7471C12.8273 11.9533 12.8835 12.2681 12.7148 12.601Z" fill="#414752"/>
                    </svg>
                </a>
                <a href="<?= Setting::getSettingByAlias('telegram') ?>" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
                        <path d="M21.4021 2.38476L18.1632 18.1079C17.9188 19.2176 17.2816 19.4937 16.376 18.971L11.4409 15.2276L9.05963 17.5851C8.7961 17.8563 8.5757 18.0832 8.06782 18.0832L8.42238 12.9096L17.569 4.40193C17.9667 4.03697 17.4828 3.83476 16.951 4.19972L5.64341 11.5286L0.775413 9.96026C-0.283471 9.61995 -0.302636 8.8703 0.995815 8.34751L20.0366 0.796667C20.9182 0.456361 21.6896 0.998877 21.4021 2.38476Z" fill="#414752"/>
                    </svg>
                </a>
                <a href="<?= Setting::getSettingByAlias('WhatsApp') ?>" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="20" viewBox="0 0 18 20" fill="none">
                        <path d="M15.304 3.39039C13.6205 1.65336 11.3786 0.697998 8.99598 0.697998C4.07812 0.697998 0.0763393 4.81724 0.0763393 9.87944C0.0763393 11.4965 0.486161 13.0764 1.26562 14.4702L0 19.2263L4.72902 17.9484C6.0308 18.6804 7.49732 19.065 8.99196 19.065H8.99598C13.9098 19.065 18 14.9458 18 9.88358C18 7.43106 16.9875 5.12742 15.304 3.39039ZM8.99598 17.5182C7.66205 17.5182 6.35625 17.1502 5.2192 16.4553L4.95 16.2899L2.14554 17.0468L2.89286 14.2303L2.71607 13.9408C1.97277 12.7249 1.58304 11.3228 1.58304 9.87944C1.58304 5.67335 4.90982 2.24892 9 2.24892C10.9808 2.24892 12.8411 3.04299 14.2393 4.48638C15.6375 5.92977 16.4973 7.84464 16.4933 9.88358C16.4933 14.0938 13.0821 17.5182 8.99598 17.5182ZM13.0621 11.8026C12.8411 11.6868 11.7442 11.1326 11.5393 11.0581C11.3344 10.9796 11.1857 10.9423 11.0371 11.1739C10.8884 11.4055 10.4625 11.9184 10.3299 12.0755C10.2013 12.2286 10.0688 12.2492 9.84777 12.1334C8.53795 11.4593 7.67813 10.9299 6.81429 9.40383C6.58527 8.99852 7.0433 9.02747 7.4692 8.15068C7.54152 7.99766 7.50536 7.86531 7.44911 7.74951C7.39286 7.63371 6.94688 6.50464 6.76205 6.04557C6.58125 5.5989 6.39643 5.66094 6.25982 5.65267C6.13125 5.6444 5.98259 5.6444 5.83393 5.6444C5.68527 5.6444 5.4442 5.7023 5.23929 5.92977C5.03437 6.16137 4.45982 6.71557 4.45982 7.84464C4.45982 8.97371 5.25938 10.0656 5.36786 10.2186C5.48036 10.3716 6.93884 12.6876 9.17679 13.6844C10.5911 14.313 11.1455 14.3668 11.8527 14.2592C12.2826 14.1931 13.1705 13.705 13.3554 13.1674C13.5402 12.6297 13.5402 12.1707 13.4839 12.0755C13.4317 11.9721 13.283 11.9142 13.0621 11.8026Z" fill="#414752"/>
                    </svg>
                </a>
            </div>
            <a class="mail" href="<?= Setting::getSettingByAlias('email') ?>"><?= Setting::getSettingByAlias('email') ?></a>
            <div class="soc">
                <a href="<?= Setting::getSettingByAlias('instagram') ?>" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" width="21" height="22" viewBox="0 0 21 22" fill="none">
                        <path d="M10.0045 5.57343C7.16615 5.57343 4.87673 7.97206 4.87673 10.9458C4.87673 13.9195 7.16615 16.3182 10.0045 16.3182C12.8428 16.3182 15.1323 13.9195 15.1323 10.9458C15.1323 7.97206 12.8428 5.57343 10.0045 5.57343ZM10.0045 14.4385C8.17028 14.4385 6.67077 12.8722 6.67077 10.9458C6.67077 9.01942 8.16581 7.45306 10.0045 7.45306C11.8432 7.45306 13.3382 9.01942 13.3382 10.9458C13.3382 12.8722 11.8387 14.4385 10.0045 14.4385ZM16.538 5.35368C16.538 6.05035 16.0025 6.60676 15.342 6.60676C14.677 6.60676 14.146 6.04568 14.146 5.35368C14.146 4.66167 14.6815 4.10059 15.342 4.10059C16.0025 4.10059 16.538 4.66167 16.538 5.35368ZM19.9342 6.62546C19.8584 4.94689 19.4924 3.46002 18.3187 2.23499C17.1494 1.00996 15.7303 0.626555 14.1281 0.542392C12.4769 0.444203 7.52763 0.444203 5.8764 0.542392C4.27871 0.621879 2.85954 1.00529 1.68582 2.23032C0.512107 3.45535 0.15062 4.94222 0.0702891 6.62079C-0.0234297 8.35079 -0.0234297 13.5361 0.0702891 15.2661C0.146157 16.9447 0.512107 18.4316 1.68582 19.6566C2.85954 20.8816 4.27425 21.265 5.8764 21.3492C7.52763 21.4474 12.4769 21.4474 14.1281 21.3492C15.7303 21.2697 17.1494 20.8863 18.3187 19.6566C19.4879 18.4316 19.8539 16.9447 19.9342 15.2661C20.0279 13.5361 20.0279 8.35547 19.9342 6.62546ZM17.801 17.1224C17.4529 18.0388 16.779 18.7448 15.8999 19.1142C14.5833 19.6613 11.4594 19.535 10.0045 19.535C8.54962 19.535 5.42119 19.6566 4.10913 19.1142C3.23442 18.7495 2.56053 18.0435 2.20797 17.1224C1.68582 15.7431 1.80632 12.4701 1.80632 10.9458C1.80632 9.42153 1.69029 6.14387 2.20797 4.76922C2.55607 3.85278 3.22995 3.14675 4.10913 2.77737C5.42565 2.23032 8.54962 2.35656 10.0045 2.35656C11.4594 2.35656 14.5878 2.23499 15.8999 2.77737C16.7746 3.14208 17.4484 3.8481 17.801 4.76922C18.3232 6.14854 18.2027 9.42153 18.2027 10.9458C18.2027 12.4701 18.3232 15.7477 17.801 17.1224Z" fill="#414752"/>
                    </svg>
                </a>
                <a href="<?= Setting::getSettingByAlias('facebook') ?>" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="22" viewBox="0 0 12 22" fill="none">
                        <path d="M10.904 12.2553L11.4595 8.46314H7.98644V6.00229C7.98644 4.96483 8.4716 3.95356 10.0271 3.95356H11.606V0.724944C11.606 0.724944 10.1732 0.46875 8.80324 0.46875C5.94309 0.46875 4.07357 2.28502 4.07357 5.57298V8.46314H0.894287V12.2553H4.07357V21.4226H7.98644V12.2553H10.904Z" fill="#414752"/>
                    </svg>
                </a>
            </div>
        </div>
    </div>
</footer>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
