<?php

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Коули');

use mihaildev\elfinder\InputFile;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url; ?>
<? //= \frontend\widgets\InstagramWidget::widget() ?>
<main class="page-home">
    <section class="main-screen">
        <div class="container">
            <div class="txt">
                <h1><?= $kouli->title ?></h1>
                <?= $kouli->text ?>
            </div>
            <div class="bg">
                <img src="/images/main.png" alt="">
                <img src="/images/main_hover.png" alt="">
            </div>
        </div>
    </section>
    <section class="about-main">
        <div class="container">
            <h2><?= $about_technique->title ?></h2>
            <div class="center">
                <?= $about_technique->text ?>
            </div>
            <a href="<?= Url::to(['site/signup']); ?>" class="btn"><p> <?= Yii::t('app', 'Записаться') ?></p></a>
        </div>
    </section>
    <section class="target-main">
        <div class="container txt-content">
            <h2><?= $purpose_wellness_project->title ?></h2>
            <?= $purpose_wellness_project->text ?>

            <h3><?= $what_will_it_give->title ?></h3>
            <?= $what_will_it_give->text ?>
        </div>
    </section>
    <section class="reviews">
        <div class="container">
            <h2>Отзывы</h2>
            <div class="rev-slider">
                <?php foreach ($reviews as $review) { ?>
                    <div class="box">
                        <div class="top">
                            <img src="<?= $review->image ?>" alt="">
                            <p>
                                <?= $review->author ?>
                            </p>
                        </div>
                        <p><?= $review->text ?></p>
                    </div>
                <?php } ?>
            </div>
            <a href="##" class="btn purple"><p>Оставить отзыв</p></a>
        </div>
    </section>

    <div class="review-popup">

        <?php $form = ActiveForm::begin([
                'fieldConfig' => [
                        'options' => [
                                'class' => 'input-box'
                        ]]
            ,
                    'options' => [
                            'class' => 'form',
                        'enctype' => 'multipart/form-data'
                    ]

        ]) ?>

        <h2>Оставить отзыв</h2>

        <?= $form->field($model, 'author')
            ->textInput(['placeholder' => $model->getAttributeLabel('author')])
            ->label(false) ?>

        <?= $form->field($model, 'status')
            ->hiddenInput(['value' => \backend\models\Review::STATUS_NOT_ACTIVE])
            ->label(false)
        ?>

        <?= $form->field($model, 'text')
            ->textarea(['placeholder' => $model->getAttributeLabel('message')])
            ->label(false) ?>

        <?= $form->field($model, 'image')->fileInput() ?>
        <!--        --><?//= $form->field($model, 'image')->widget(InputFile::className(), [
        //            'language'      => 'ru',
        //            'controller'    => 'elfinder',
        //            'filter'        => 'image',
        //            'path'          => 'reviews',
        //            'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
        //            'options'       => ['class' => 'form-control'],
        //            'buttonOptions' => ['class' => 'btn btn-default'],
        //            'multiple'      => false
        //        ])->label($model->getAttributeLabel('image') ); ?>


        <button class="btn purple" ><p><?= Yii::t('app', 'Отправить') ?></p></button>

        <?php ActiveForm::end() ?>
        <div class="exit-btn"></div>
    </div>
</main>

