<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = $page->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<main class="page-contacts">
    <section class="page-header-inside">
        <div class="container">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <p>-</p>
                <p><?= $this->title ?></p>
            </div>
            <h1><?= $this->title ?></h1>
        </div>
    </section>
    <section class="request-cont">
        <div class="container">
            <div class="left">
                <?php $form = ActiveForm::begin(['fieldConfig' => ['options' => ['class' => 'input-box']]]) ?>

                <?= $form->field($model, 'name')
                    ->textInput(['placeholder' => $model->getAttributeLabel('name')])
                    ->label(false) ?>

                <?= $form->field($model, 'phone')
                    ->textInput(['placeholder' => $model->getAttributeLabel('phone')])
                    ->label(false) ?>

                <?= $form->field($model, 'email')
                    ->textInput(['placeholder' => $model->getAttributeLabel('email')])
                    ->label(false) ?>

                <?= $form->field($model, 'time')
                    ->textInput(['placeholder' => $model->getAttributeLabel('time')])
                    ->label(false) ?>

                <?= $form->field($model, 'subject')
                    ->hiddenInput(['value' => 'Форма записи'])
                    ->label(false)
                ?>

                <?= $form->field($model, 'message')
                    ->textarea(['placeholder' => $model->getAttributeLabel('message')])
                    ->label(false) ?>


                <button class="btn purple" ><p><?= Yii::t('app', 'Записаться') ?></p></button>
                <!--                --><?//= Html::submitButton(Yii::t('app', 'Відправити'), ['class' => 'btn purple']) ?>

                <?php ActiveForm::end() ?>

            </div>
            <div class="right">
                <?= $page->text ?>
            </div>
        </div>
    </section>
</main>

<?php
$js = <<< JS
jQuery(function($){
   $("#contactform-phone").mask("+38(999) 999-9999");
   });
JS;

$this->registerJs( $js, $position = yii\web\View::POS_END, $key = null );
?>
