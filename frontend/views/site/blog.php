<?php
$this->title = Yii::t('app', 'Blog');
?>

<section id="container" class="zerogrid">
    <div class="wrap-container">

        <?php $i = 1; foreach ($people as $person): ?>
            <article class="archive-post">
                <div class="row wrap-post"><!--Start Post-->
                    <div class="flex-box">
                        <div class="col-1-2 <?= ( $i % 2 ) == 0 ? 'f-right' : '' ?>">
                            <div class="box-image" style="background-image: url( <?= $person->image ?> )"></div>
                        </div>
                        <div class="col-1-2">
                            <div class="box-text text-right">
                                <div class="heading">
                                    <h2><?= $person->name ?></h2>
                                </div>
                                <div class="content">
                                    <?= $person->text ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        <?php $i++; endforeach; ?>
    </div>
</section>