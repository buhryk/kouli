<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $page->title;

?>
<main class="page-history">
    <section class="page-header-inside">
        <div class="container">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <p>-</p>
                <p><?= $page->title ?></p>
            </div>
            <h1><?= $page->title ?></h1>
        </div>
    </section>
    <section class="content">
        <div class="container txt-content">
            <?= $page->text ?>
        </div>
    </section>
    <section class="dop-hist">
        <div class="container">
            <a href="<?= Url::to(['site/signup']); ?>" class="btn purple"><p><?= Yii::t('app', 'Записаться') ?></p></a>
        </div>
    </section>
</main>
