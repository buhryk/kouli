<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = $page->title;

?>
<main class="page-teacher">
    <section class="page-header-inside">
        <div class="container">
            <div class="breadcrumbs">
                <a href="##">Главная</a>
                <p>-</p>
                <p><?= $page->title ?></p>
            </div>
            <h1><?= $page->title ?></h1>
        </div>
    </section>
    <section class="teacher-cont">
        <div class="container">
            <div class="box">
                <?= $page->text ?>
            </div>
        </div>
    </section>
</main>
