<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Contact');

?>
<main class="page-contacts">
    <section class="page-header-inside">
        <div class="container">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <p>-</p>
                <p><?= $page->title ?></p>
            </div>
            <h1><?= $page->title ?></h1>
        </div>
    </section>
    <section class="contacts-content">
        <div class="container">
            <div class="left">

                <?= $page->text ?>
                <a href="<?= Url::to(['site/signup']); ?>" class="btn purple"><p>Записаться</p></a>
            </div>
            <div class="right">
                <?php $form = ActiveForm::begin(['fieldConfig' => ['options' => ['class' => 'input-box']]]) ?>

                <?= $form->field($model, 'name')
                    ->textInput(['placeholder' => $model->getAttributeLabel('name')])
                    ->label(false) ?>

                <?= $form->field($model, 'phone')
                    ->textInput(['placeholder' => $model->getAttributeLabel('phone')])
                    ->label(false) ?>

                <?= $form->field($model, 'email')
                    ->textInput(['placeholder' => $model->getAttributeLabel('email')])
                    ->label(false) ?>

                <?= $form->field($model, 'subject')
                    ->hiddenInput(['value' => 'Контактная форма'])
                    ->label(false)
                ?>

                <?= $form->field($model, 'message')
                    ->textarea(['placeholder' => $model->getAttributeLabel('message')])
                    ->label(false) ?>


                <button class="btn purple" ><p><?= Yii::t('app', 'Отправить') ?></p></button>
<!--                --><?//= Html::submitButton(Yii::t('app', 'Відправити'), ['class' => 'btn purple']) ?>

                <?php ActiveForm::end() ?>

            </div>
        </div>
    </section>
</main>
<?php
$js = <<< JS
jQuery(function($){
   $("#contactform-phone").mask("+38(999) 999-9999");
   });
JS;

$this->registerJs( $js, $position = yii\web\View::POS_END, $key = null );
?>


