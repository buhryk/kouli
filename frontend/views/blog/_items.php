<?php
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<?php foreach ($dataProvider->models as $model) { ?>
    <?= $this->render('_one_item', ['model' => $model]); ?>
<?php } ?>