<?php
use yii\helpers\Url;

/* @var $model backend\modules\blog\models\Item */
?>

<?php echo \yii2mod\comments\widgets\Comment::widget([
    'model' => $model,
    'commentView' => '@frontend/views/comments/index' // path to your template
]); ?>