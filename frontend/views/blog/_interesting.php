<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="interest clearfix">
    <h2 class="interest__title border-line-wrap">
        <?= Yii::t('blog', 'Так же вам будет интересно'); ?> <span class="border-line border-line_lrg"></span>
    </h2>

    <div class="slider">
        <div class="slider-controls">
            <span class="slider-control-item slider-control-item__prev control-slider__prev icon-left-circle"></span>
            <span class="slider-control-item slider-control-item__next control-slider__next icon-right-circle"></span>
        </div>
        <ul id="light-slider" class="slider-content">
            <?php foreach ($models as $model) { ?>
                <li>
                    <p class="date__attr">
                        <?= Yii::t('blog', 'дата статьи'); ?>:
                        <?= Html::a(date('d.m.Y', $model->published_at), ['view', 'slug' => $model->alias], ['class' => 'date__attr-item']); ?>
                    </p>
                    <a href="<?= Url::to(['view', 'slug' => $model->alias]); ?>" class="slider-content__img-link">
                        <?= Yii::$app->thumbnail->img($model->preview_image, ['thumbnail' => ['width' => 290, 'height' => 244]], ['alt' => $model->title]); ?>
                        <span class="slider-content__img-search"></span>
                    </a>
                    <h2 class="interest-slider__title">
                        <?= Html::a($model->title, ['view', 'slug' => $model->alias]); ?>
                    </h2>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>