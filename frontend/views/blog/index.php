<?php
use frontend\components\CustomPagination;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Блог';
?>

<main class="page-blog">
    <section class="page-header-inside">
        <div class="container">
            <div class="breadcrumbs">
                <a href="/">Главная</a>
                <p>-</p>
                <p><?= $this->title ?></p>
            </div>
            <h1><?= $this->title ?></h1>
        </div>
    </section>
    <?php if ($actual) { ?>
        <section class="blog-block">
            <div class="container">
                <h2>Актуальное</h2>
                <div class="blog-grid">
                    <?php

                    foreach ($actual->models as $model) { ?>
                        <div class="box">
                        <img src="<?= $model->preview_image ?>" alt="">
                        <h3><?= $model->title ?></h3>
                        <div class="row">
                            <p><span>Категория:</span> <?= $model->category->title ?></p>
                            <p><span>Автор:</span> <?= $model->author->name ?></p>
                        </div>
                        <p><?= $model->short_description ?></p>
                        <a href="<?= Url::to(['view', 'slug' => $model->alias]); ?>" class="btn transparent"><p>Читать далее</p></a>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </section>
    <?php } ?>
    <section class="blog-block">
        <div class="container">
            <h2>Все новости</h2>
            <div class="blog-grid">
                <?php foreach ($models as $model) { ?>
                    <div class="box">
                        <img src="<?= $model->preview_image ?>" alt="">
                        <h3><?= $model->title ?></h3>
                        <div class="row">
                            <p><span>Категория:</span> <?= $model->category->title ?></p>
                            <p><span>Автор:</span> <?= $model->author->name ?></p>
                        </div>
                        <p><?= $model->short_description ?></p>
                        <a href="<?= Url::to(['view', 'slug' => $model->alias]); ?>" class="btn transparent"><p>Читать далее</p></a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <section class="pagination">
        <div class="container">

<?php
            echo LinkPager::widget([
                'pagination' => $pagination,
                'options' => ['class' => ''],
                'firstPageLabel' => '<<',
                'lastPageLabel' => '>>',
                'prevPageLabel' => '<',
                'nextPageLabel' => '>',
                'activePageCssClass' => 'active',
                'maxButtonCount' => 3,
                'linkOptions' => ['class' => ''],
            ]);
            ?>
        </div>

    </section>
</main>