<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\StringHelper;
?>

<div class="col-md-6 col-lg-4 blog-item">
    <p class="date__attr date__attr_floatl">
        <?= Yii::t('blog', 'дата статьи'); ?>:
        <?= Html::a(date('d.m.Y', $model->published_at), ['view', 'slug' => $model->alias], ['class' => 'date__attr-item']); ?>
    </p>
    <ul class="blog-item__action clearfix">
        <li>
            <img src="/image/ico/eye.png" class="blog-item__icon blog-item__icon-eye" alt="like">
            <span><?= $model->views; ?></span>
        </li>
        <li>
            <img src="/image/ico/post-like.png" class="blog-item__icon blog-item__icon-like" alt="repost">
            <span><?= $model->likes; ?></span>
        </li>
        <?php if ($model->ratio) { ?>
            <li>
                <img src="/image/ico/favorite.png" class="blog-item__icon blog-item__icon-favorite" alt="favorite">
                <span><?= $model->ratio; ?></span>
            </li>
        <?php } ?>
    </ul>
    <a href="<?= Url::to(['view', 'slug' => $model->alias]); ?>" class="blog-item__photo">
        <?= Yii::$app->thumbnail->img($model->preview_image, ['thumbnail' => ['width' => 465, 'height' => 331]], ['alt' => $model->title]); ?>
    </a>
    <div class="blog-item-info">
        <span class="blog-item__micro-line blog-item__micro-line_bg"></span>
        <h5 class="blog-item__title">
            <?= Html::a($model->title, ['view', 'slug' => $model->alias]); ?>
        </h5>

        <?php $category = $model->category; ?>
        <?php if ($category) { ?>
            <div class="blog-item__category">
                <div class="blog-item__category">
                    <?= Yii::t('blog', 'категория'); ?>: <a><?= $category->title; ?></a>
                </div>
            </div>
        <?php } ?>

        <?php $tags = $model->tags; ?>
        <?php if ($tags) { ?>
            <?php $tagsLinksArray = []; ?>
            <?php foreach ($tags as $tag) {
                $tagsLinksArray[] = Html::a($tag->title, null);
            } ?>
            <div class="blog-item__tag">
                <div class="blog-item__tag">
                    <?= Yii::t('blog', 'теги'); ?>:
                    <?= implode(', ', $tagsLinksArray); ?>
                </div>
            </div>
        <?php } ?>

        <?php $author = $model->author; ?>
        <?php if ($author) { ?>
            <p class="blog-item__author">
                <?= Yii::t('blog', 'Автор'); ?>: <span><?= $author->name; ?></span>
            </p>
        <?php } ?>

        <p class="blog-item__content">
            <?= StringHelper::truncate($model->short_description, 200); ?>
        </p>
        <?= Html::a(Yii::t('blog', 'читаем'), ['view', 'slug' => $model->alias], ['class' => 'blog-item__btn']); ?>
    </div>
</div>