$(document).ready(function () {
    if ($('main').hasClass('page-home')){
        $('.rev-slider').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            speed: 500,
            responsive: [
                {
                    breakpoint: 1031,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 701,
                    settings: {
                        slidesToShow: 1,
                        fade: true,
                    }
                },
            ]
        });
    }
    $('.alert-success .close').click(function (e) {
        e.preventDefault();
        $(this).closest('.alert-success').fadeOut();
    });
    $('.reviews .btn').click(function (e) {
        e.preventDefault();
        $('.review-popup').fadeIn();
    });
    $('.review-popup .exit-btn').click(function (e) {
        e.preventDefault();
        $(this).closest('.review-popup').fadeOut();
    });
    $('.menu-btn').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('open');
        $('.header .menu').fadeToggle();
        $('.header').toggleClass('open');
        $('body').toggleClass('fix');
    });
});