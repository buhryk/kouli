<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',

    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'i18n' => [
            'class' => Zelenin\yii\modules\I18n\components\I18N::className(),
            'languages' => ['ru-RU', 'en-EN', 'ua']
        ],
        'thumbnail' => [
            'class' => 'common\components\Thumbnail',
            'basePath' => '@frontend/web',
            'prefixPath' => '/',
            'cachePath' => 'uploads/thumbnails'
        ],
    ],
];

function pr($data) {
    echo '<pre>';
    print_r($data);
    exit('-----------------------EXIT------------------------------');
}
