<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<p><strong>От кого </strong>: <?= Html::encode($contact->name) ?> </p>
<p><strong>Email</strong>: <?= Html::encode($contact->email) ?></p>
<p><strong>Телефон</strong>: <?= Html::encode($contact->phone) ?></p>
<?php if ($contact->time) { ?>
    <p><strong>Удобное время</strong>: <?= Html::encode($contact->time) ?></p>
<?php } ?>
<p><strong>Текст сообщения</strong>:</p>
<p><?= Html::encode($contact->message) ?>.</p>


