<?php
use yii\helpers\Html;
?>


    <li id="language-menu"><a><?= $current->name;?></a>
        <ul>
            <?php foreach ($langs as $lang):?>
            <li>
                <?= Html::a(
                    $lang->name,
                    ( ($lang->url == 'ru') ? '' : '/'.$lang->url ).Yii::$app->getRequest()->getLangUrl(),
                    ['style' => ($true) ? '' : 'pointer-events: none; cursor: default;  color: #999']
                ) ?>
            </li>
            <?php endforeach;?>
        </ul>
    </li>


