<?php
namespace common\widgets;
use backend\models\People;
use common\models\Lang;
use Yii;

class WLang extends \yii\bootstrap\Widget
{
    public function init(){}

    public function run()
    {

        return $this->render('lang/view', [
            'current' => Lang::getCurrent(),
            'langs' => Lang::find()->where(
                'id != :current_id', [':current_id' => Lang::getCurrent()->id],

            )
                ->andWhere(['status' => Lang::STATUS_ACTIVE])
                ->all(),
            'true' => true
        ]);
    }
}