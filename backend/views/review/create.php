<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Review */

$this->title = Yii::t('app', 'Создать отзыв');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Отзывы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
