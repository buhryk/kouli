<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Review as ModelData;


/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Отзывы');
$this->params['breadcrumbs'][] = $this->title;

\backend\widgets\SortActionWidget::widget(['className' => ModelData::className()]);
?>
<div class="review-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <span>
        <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i>' .Yii::t('app', ' Добавить'), ['create'], ['class' => 'btn btn-success']) ?>
    </span>

    <div class="pull-right">
        <?= \backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => ModelData::className()]) ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'class'=>'table table-custom dataTable no-footer',
        'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'checkbox-item'],
            ],
            [
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'sort-item'],
                'value' => function() {
                    return '<i class="fa fa-arrows-alt"> </i>';
                }
            ],
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'author',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img($data->image,[
                        'alt'=> 'yii2 - картинка в gridview',
                        'style' => 'width:150px;'
                    ]);
                },

            ],
            [
                'attribute' => 'status',
                'filter' => $searchModel::getStatusList(),
                'value' => 'statusDetail'
            ],
            'updated_at:datetime',
            //'text:ntext',

            //'position',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
