<?php
use common\widgets\WLang;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Menu;

$user = Yii::$app->user->identity;
?>
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="<?=Url::to(['/']) ?>" class="site_title"><img src="<?=Url::to(['images/logo.png'])?>" alt="" width="40px"> <span>Admin</span></a>
        </div>
        <div class="clearfix"></div>
        <br />
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Меню</h3>
                <?= Menu::widget([
                        'options' => ['class' => 'nav side-menu'],
                        'submenuTemplate' => "\n<ul class='nav child_menu'>\n{items}\n</ul>\n",
                        'encodeLabels' => false,
                        'activateParents' => true,
                        'items' => [
                            ['label' => '<i class="fa fa-comments"></i> Блог <span class="fa fa-chevron-down"></span>', 'url' => '#',
                                'items' => [
                                    ['label' => 'Посты', 'url' => ['/blog/item']],
                                    ['label' => 'Категории', 'url' => ['/blog/category']],
                                    ['label' => 'Авторы', 'url' => ['/blog/author']
                                    ],
                                ]],
                            ['label' => '<i class="fa fa-folder"></i> Страницы ', 'url' => ['/core/page']],
                            ['label' => '<i class="fa fa-user"></i> Отзывы ', 'url' => ['/review']],
                            ['label' => '<i class="fa fa-cogs"></i> Настройки ', 'url' => ['/setting/setting']],
                            ['label' => '<i class="fa fa-italic"></i> Переводы ', 'url' => ['/translate/default/']],
                            ['label' => '<i class="fa fa-deaf"></i> Языки ', 'url' => ['/core/lang']]
                        ],
                    ]);
                ?>
            </div>
        </div>
        <!-- /sidebar menu -->
        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip"  data-method="post" data-placement="top" title="Logout" href="<?=Url::to(['/site/logout']) ?>">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?=Url::to(['/site/logout']) ?>" data-method="post"><i class="fa fa-sign-out pull-right"></i></a></li>
                <li style="float: right"><?= \backend\widgets\WLangHeader::widget([]); ?></li>
            </ul>
        </nav>
    </div>
</div>
<!-- top navigation -->