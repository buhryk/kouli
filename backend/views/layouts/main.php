<?php
/**
 * Created by PhpStorm.
 * User: Serhii_Bugryk
 * Date: 09.12.2017
 * Time: 13:18
 */
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;
use common\widgets\Alert;
use backend\assets\ToastrAsset;
use backend\widgets\MainModal;


ToastrAsset::register($this);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="<?= isset($_COOKIE['menu_type']) && $_COOKIE['menu_type'] == 1 ? 'nav-sm':'nav-md' ?>   ">
    <?php $this->beginBody() ?>
    <div class="container body">
        <div class="main_container">
            <?=$this->render('header'); ?>
            <!-- page content -->
            <div class="right_col" role="main">
                <div class="" style="padding-left: 8px">
                    <h3><?=$this->title ?></h3>
                </div>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <div class="x_panel">
                    <?= $content ?>
                </div>
            </div>
            <!-- /page content -->
            <?=$this->render('footer') ?>
        </div>
    </div>
    <?= MainModal::widget() ?>
    <?= Alert::widget() ?>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>