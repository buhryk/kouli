<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\People */

$this->title = Yii::t('app', 'Створити People');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Peoples'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="people-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
