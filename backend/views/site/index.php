<?php
use yii\helpers\Url;

//HomeAsset::register($this);
/* @var $this yii\web\View */

$this->title = Yii::$app->name;

?>


<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <a href="<?= Url::to(['/people']) ?>">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-users"></i></div>
            <div class="count"><?= $people ?></div>
            <h3>Користувачі</h3>
        </div>
    </a>
</div>

<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
<a href="<?= Url::to(['/email']) ?>">
    <div class="tile-stats">
        <div class="icon"><i class="fa fa-user"></i></div>
        <div class="count"><?= $emails ?></div>
        <h3>Подписчики</h3>
    </div>
</a>
</div>

<div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <a href="<?= Url::to(['/form']) ?>">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-wpforms"></i></div>
            <div class="count"><?= $forms ?></div>
            <h3>Формы</h3>
        </div>
    </a>
</div>
