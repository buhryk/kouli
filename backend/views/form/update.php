<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Form */

$this->title = Yii::t('app', 'Редагувати : {nameAttribute}', [
    'nameAttribute' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Forms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редагувати');
?>
<div class="form-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
