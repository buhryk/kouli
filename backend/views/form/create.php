<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Form */

$this->title = Yii::t('app', 'Створити Form');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Forms'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
