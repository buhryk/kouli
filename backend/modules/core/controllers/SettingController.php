<?php

namespace backend\modules\core\controllers;

use backend\controllers\BaseController;
use backend\modules\core\models\Setting;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\filters\AccessControl;


class SettingController extends BaseController
{

    public function actionIndex()
    {
        $models = Setting::find()->indexBy('key')->all();

        if (Model::loadMultiple($models, Yii::$app->request->post()) && Model::validateMultiple($models)) {
            foreach ($models as $item) {
                $item->save(false);
            }
            Yii::$app->session->setFlash('success', 'Зміни збережені');
            return $this->refresh();
        }

        return $this->render('index', [
            'settings' => $models,
        ]);
    }

    public function actionCreate()
    {
        $count = count(Yii::$app->request->post('Setting', []));
        $settings = [new Setting()];
        for($i = 1; $i < $count; $i++) {
            $settings[] = new Setting();
        }

        // ...
    }
}
