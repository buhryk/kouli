<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Редагування налаштувань';
$this->params['breadcrumbs'][] = $this->title;

$form = ActiveForm::begin(['options' => ['id'=> 'form-common-info']]);
?>
<div class="row">
    <div class="col-md-8">
    <?php foreach ($settings as $index => $setting):?>
        <div class="col-md-12">
            <div class="form-group">
                <label for="input02" class="col-sm-4 control-label">Відправляти контактну форму на наступні email </label>
                <div class="col-sm-8">
                    <?=$form->field($setting, "[$index]value")->label(false); ?>
                </div>
            </div>
        </div>
    <?php endforeach;?>
        <div class="col-md-12">
            <div class="form-group">
                <button class="btn btn-success">
                    Зберегти
                </button>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end();?>