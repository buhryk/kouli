<?php
use yii\helpers\Url;
?>
<tr>
    <td class="checkbox-item" style="width: 10px;" >
        <input type="checkbox" name="selection[]" value="<?=$model->id ?>">
    </td>
    <td  style="padding-left: <?= $level ? $level * 25 : 10 ?>px" class="sort-item">
        <i class="fa fa-arrows-alt"></i> <?=$model->title ?>
    </td>
    <td>
        <?=$model->statusDetail ?>
    </td>
    <td>
        <?=Yii::$app->formatter->asDatetime($model->updated_at) ?>
    </td>
    <td style="">
        <a class="btn btn-info btn-xs" href="<?=Url::to(['update', 'id' => $model->id]) ?>" title="Редагувати">
            <i class="fa fa-pencil"></i></a>
        <a class="btn btn-danger btn-xs" href="<?=Url::to(['delete', 'id' => $model->id]) ?>" title="Видалити" onclick="return confirm(&quot;Ви дійсно хочете видалити даний запис?&quot;)">
            <i class="fa fa-trash-o"></i>
        </a>
    </td>
</tr>

<?php foreach ($model->childrens as $item) :
    echo $this->render('_view', ['model' => $item, 'level' => $level + 1]) ?>
<?php endforeach; ?>