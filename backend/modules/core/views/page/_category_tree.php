<?php
use yii\helpers\Url;
?>
<div id="using_json"></div>

<?= $this->registerJs("
var urlRedirect = '".Url::to(['/core/page/index'])."';
$('#using_json').jstree({ 'core' : {
    'data' : ".json_encode($categoriesTree)."
} });

$('body').on('click', '#using_json li a', function() {
     var category_id = $(this).parent().attr('id'); 
     url = urlRedirect+ '?category_id=' + category_id;
     window.location.href = url;
});
", \yii\web\View::POS_READY) ?>
