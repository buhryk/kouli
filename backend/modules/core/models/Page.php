<?php

namespace backend\modules\core\models;

use backend\modules\transliterator\services\TransliteratorService;
use common\behaviors\LangBehavior;
use common\models\BaseDataModel;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use common\models\Lang;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $image
 * @property string $alias
 * @property integer $category_id
 * @property string $additional_data
 *
 * @property PageCategory $category
 * @property PageLang[] $pageLangs
 * @property Lang[] $langs
 */
class Page extends BaseDataModel
{
    public $title;
    public $record_id;
    public $description;
    public $text;
//    public $additional_title;

    static $current = null;

    public static function tableName()
    {
        return 'page';
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'alias',
                'ensureUnique' => true
            ],
            [
                'class' => LangBehavior::className(),
                't' => new PageLang(),
                'fk' => 'record_id',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['alias', 'image'], 'string', 'max' => 255],
            [['title', 'text'], 'required'],
            ['alias', 'unique'],
            [['description', 'record_id'], 'safe']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'alias' => 'Alias',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'text' => 'Контент',
            'category_id' => 'Категория',
//            'additional_title' => 'Додатковий заголовок'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasMany(PageLang::className(), ['record_id' => 'id'])->where(['lang_id' => Lang::getCurrent()->id]);
    }

    static public function getPageAll($map = false)
    {
        $query = self::find()->where(['status' => self::STATUS_ACTIVE])->orderBy(['position' => SORT_ASC]);

        $models = $query->all();
        if ($map) {
            return ArrayHelper::map($models, 'id', 'title');
        }
        return $models;
    }

    public function beforeSave($insert)
    {
        if (!$this->alias)  $this->alias = TransliteratorService::transliterate($this->title);

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }

    public static function getPageById($id)
    {
        $model = Page::find()
            ->where(['id' => $id, 'status' => Page::STATUS_ACTIVE])
            ->joinWith('lang')
            ->one();

        if (! $model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model->registerData();

        return $model;
    }


    public function registerData()
    {
        self::$current = $this;

        $view = Yii::$app->view;
        $view->params['pageH1'] = $this->title;
//        $view->title = $this->title . ' | '. Yii::t('common', 'Український інститут експертизи сортів рослин');
        $view->params['pageImage'] = $this->getImage();
    }

    public static function getPageByAlias($alias)
    {
        $model = Page::find()
            ->where(['alias' => $alias, 'status' => Page::STATUS_ACTIVE])
            ->joinWith('lang')
            ->one();

//        if (! $model) {
//            throw new NotFoundHttpException('The requested page does not exist.');
//        }

        return $model;
    }

    public static function getCurrent()
    {
        if (self::$current == null) {
            self::$current = self::find()
                ->where(['alias' => Yii::$app->request->url])
                ->one();
        }

        return self::$current;
    }
}
