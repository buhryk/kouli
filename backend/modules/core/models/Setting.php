<?php

namespace backend\modules\core\models;

use Yii;

/**
 * This is the model class for table "setting".
 *
 * @property integer $id
 * @property string $key
 * @property string $group
 * @property string $value
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $additional_data
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group'], 'required'],
            [['value', 'additional_data'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['key', 'group'], 'string', 'max' => 255],
            [['key'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'group' => 'Group',
            'value' => 'Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'additional_data' => 'Additional Data',
        ];
    }

    public static function getByKey($key)
    {
        return  self::find()->andWhere(['key' => $key])->one();
    }

}
