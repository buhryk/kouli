<?php

namespace backend\modules\core\models;

use Yii;
use common\models\Lang;

/**
 * This is the model class for table "page_lang".
 *
 * @property integer $record_id
 * @property integer $lang_id
 * @property string $title
 * @property string $description
 * @property string $text
 */
class PageLang extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'page_lang';
    }

    public function rules()
    {
        return [
            [['title', 'text'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['description', 'text'], 'string'],
            [['title'], 'string', 'max' => 128],
//            [['additional_title'], 'string', 'max' => 250],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['record_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'record_id' => 'Record ID',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
            'description' => 'Description',
            'text' => 'Text',
        ];
    }

    public function getRecord()
    {
        return $this->hasOne(Page::className(), ['id' => 'record_id']);
    }
}
