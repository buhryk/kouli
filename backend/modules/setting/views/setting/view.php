<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\setting\models\Setting */

$this->title = $model->alias;
$this->params['breadcrumbs'][] = ['label' => 'Настройки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i>Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данную запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('alias'); ?></th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('value'); ?></th>
            <td><?= $model->value; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('description'); ?></th>
            <td><?= $model->description; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('created_at'); ?></th>
            <td><?= date('Y-m-d H:i:s', $model->created_at) ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('updated_at'); ?></th>
            <td><?= date('Y-m-d H:i:s', $model->updated_at) ?></td>
        </tr>
        </tbody>
    </table>
</div>

