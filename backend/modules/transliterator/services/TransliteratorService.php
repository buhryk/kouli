<?php
namespace backend\modules\transliterator\services;

class TransliteratorService
{
    const DEFAUL_LANG_FOR_TRANSLIT = 'ru-RU';

    public static function transliterate($string, $replacement = '-')
    {
        $roman = array("Sch","sch",'Yo','Zh','Kh','Ts','Ch','Sh','Yu','ya','yo','zh','kh','ts','ch','sh','yu','ya','A',
            'B','V','G','D','E','Z','I','Y','K','L','M','N','O','P','R','S','T','U','F','','Y','','E','a','b','v','g',
            'd','e','z','i','y','k','l','m','n','o','p','r','s','t','u','f','','y','','e','I','i');

        $cyrillic = array("Щ","щ",'Ё','Ж','Х','Ц','Ч','Ш','Ю','Я','ё','ж','х','ц','ч','ш','ю','я','А','Б','В','Г','Д',
            'Е','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Ь','Ы','Ъ','Э','а','б','в','г','д','е','з','и',
            'й','к','л','м','н','о','п','р','с','т','у','ф','ь','ы','ъ','э','І', 'і');

        $trans = array(' ' => $replacement, '.' => $replacement, ',' => $replacement, '/' => $replacement);

        return strtr(mb_strtolower(str_replace($cyrillic, $roman, $string)), $trans);
    }

    public static function transliterateUnique($models, $field, $alias)
    {
        $alias = self::transliterate($alias);

        if (!$models) {
            return $alias;
        }

        foreach ($models as $model) {
            if ($model->$field == $alias) {
                return self::getUniqueByIteration($models, $field, $alias, 1);
            }
        }

        return $alias;

    }

    private function getUniqueByIteration($models, $field, $alias, $iteration = 1)
    {
        $alias .= $iteration;

        foreach ($models as $model) {
            if ($model->$field == $alias) {
                return self::getUniqueByIteration($models, $field, $alias, ++$iteration);
            }
        }

        return $alias;
    }
}

