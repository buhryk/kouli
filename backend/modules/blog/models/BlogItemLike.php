<?php

namespace backend\modules\blog\models;

use Yii;

/**
 * This is the model class for table "blog_item_like".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $item_id
 * @property integer $created_at
 */
class BlogItemLike extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog_item_like';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'item_id'], 'required'],
            [['user_id', 'created_at', 'item_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'item_id' => 'Item ID',
            'created_at' => 'Created At',
        ];
    }
}
