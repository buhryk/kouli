<?php

namespace backend\modules\blog\models;

use Yii;

/**
 * This is the model class for table "blog_author_lang".
 *
 * @property integer $id
 * @property integer $record_id
 * @property string $lang
 * @property string $name
 */
class AuthorLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog_author_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['record_id', 'lang', 'name'], 'required'],
            [['record_id'], 'integer'],
            [['lang'], 'string', 'max' => 5],
            [['name'], 'string', 'max' => 55],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'record_id' => 'Record ID',
            'lang' => 'Lang',
            'name' => 'Name',
        ];
    }
}
