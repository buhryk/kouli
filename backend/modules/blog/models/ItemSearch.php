<?php

namespace backend\modules\blog\models;

use backend\modules\news\models\NewsSearch;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\blog\models\Item;

/**
 * ItemSearch represents the model behind the search form about `backend\modules\blog\models\Item`.
 */
class ItemSearch extends Item
{
    public $title;
    public $tags;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'status', 'author_id', 'actual'], 'integer'],
            [['alias', 'title', 'published_at', 'created_at', 'tags'], 'safe'],
            [['created_at', 'published_at'], 'validateDateTime'],
        ];
    }

    public function validateDateTime($attribute, $params)
    {
        if ($this->$attribute && ($this->$attribute != date('Y-m-d', strtotime($this->$attribute)))) {
            $this->addError($attribute, 'Поле должно быть формата "2000-00-00"');
        }
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Item::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'author_id' => $this->author_id,
            'status' => $this->status,
            'actual' => $this->actual,
        ]);

        $query->andFilterWhere(['like', 'alias', $this->alias]);

        if ($this->created_at) {
            $query->andFilterWhere([
                'between',
                'created_at',
                strtotime($this->created_at),
                strtotime($this->created_at) + NewsSearch::ONE_DAY_IN_SECONDS]);
        }

        if ($this->published_at) {
            $query->andFilterWhere([
                'between',
                'published_at',
                strtotime($this->published_at),
                strtotime($this->published_at) + NewsSearch::ONE_DAY_IN_SECONDS]);
        }

        if ($this->tags) {
            $query->innerJoin(ItemTag::tableName(), ItemTag::tableName().'.item_id='.Item::tableName().'.id')
                ->andWhere(['in', ItemTag::tableName().'.tag_id', $this->tags]);
        }

        if ($this->title) {
            $query->innerJoin(ItemLang::tableName(), ItemLang::tableName().'.record_id='.Item::tableName().'.id')
                ->andWhere(['like', ItemLang::tableName().'.title', $this->title]);
        }

        return $dataProvider;
    }
}
