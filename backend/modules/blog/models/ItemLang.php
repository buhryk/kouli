<?php

namespace backend\modules\blog\models;

use Yii;

/**
 * This is the model class for table "blog_item_lang".
 *
 * @property integer $id
 * @property integer $record_id
 * @property string $lang
 * @property string $title
 * @property string $short_description
 * @property string $text
 */
class ItemLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog_item_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['record_id', 'lang', 'title', 'short_description', 'text'], 'required'],
            [['record_id'], 'integer'],
            [['short_description', 'text'], 'string'],
            [['lang'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 55],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'record_id' => 'Record ID',
            'lang' => 'Lang',
            'title' => 'Title',
            'short_description' => 'Short Description',
            'text' => 'Text',
        ];
    }
}
