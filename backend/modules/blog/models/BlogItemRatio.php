<?php

namespace backend\modules\blog\models;

use Yii;

/**
 * This is the model class for table "blog_item_ratio".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $item_id
 * @property integer $ratio
 * @property integer $created_at
 */
class BlogItemRatio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog_item_ratio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'item_id', 'ratio'], 'required'],
            [['user_id', 'item_id', 'ratio', 'created_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'item_id' => 'Item ID',
            'ratio' => 'Ratio',
            'created_at' => 'Created At',
        ];
    }
}
