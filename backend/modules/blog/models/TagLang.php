<?php

namespace backend\modules\blog\models;

use Yii;

/**
 * This is the model class for table "blog_tag_lang".
 *
 * @property integer $id
 * @property integer $record_id
 * @property string $lang
 * @property string $title
 */
class TagLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog_tag_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['record_id', 'lang', 'title'], 'required'],
            [['record_id'], 'integer'],
            [['lang'], 'string', 'max' => 5],
            [['title'], 'string', 'max' => 55],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'record_id' => 'Record ID',
            'lang' => 'Lang',
            'title' => 'Title',
        ];
    }
}
