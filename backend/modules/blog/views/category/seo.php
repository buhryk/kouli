<?php
use backend\widgets\SeoWidget;

$this->title = 'Редактирование SEO';
$this->params['breadcrumbs'][] = ['label' => 'Категории постов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Категория #$model->id", 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model
    ]); ?>

    <?= SeoWidget::widget([
        'model' => $seo,
        'parameters' => ['modelLang' => $seoLang]
    ]); ?>
</div>