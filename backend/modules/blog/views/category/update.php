<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\Category */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Категории постов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Категория #$model->id", 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="category-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>
</div>
