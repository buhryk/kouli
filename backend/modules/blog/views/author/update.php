<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\Author */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Авторы постов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Автор #$model->id", 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="author-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang,
    ]) ?>
</div>
