<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Авторы постов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="author-index">
    <h1>
        <?= Html::encode($this->title); ?>
        <a href="<?= Url::to(['create']); ?>" class="btn btn-success block right">
            <i class="fa fa-plus" aria-hidden="true"></i>Добавить автора
        </a>
    </h1>

    <table class="table table-bordered" style="margin-top: 15px;">
        <thead>
        <tr>
            <th width="75">ID</th>
            <th>Имя</th>
            <th>Alias</th>
            <th width="105">Действия</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($dataProvider->models as $model) { ?>
            <tr>
                <td><?= $model->primaryKey; ?></td>
                <td><?= $model->name; ?></td>
                <td><?= $model->alias; ?></td>
                <td>
                    <a href="<?= Url::to(['view', 'id' => $model->primaryKey]); ?>"
                       title="Просмотреть"
                       class="btn btn-primary btn-xs"
                    >
                        <i class="fa fa-eye"></i>
                    </a>
                    <a href="<?= Url::to(['update', 'id' => $model->primaryKey]); ?>"
                       title="Редактировать"
                       class="btn btn-info btn-xs"
                    >
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a href="<?= Url::to(['delete', 'id' => $model->primaryKey]); ?>"
                       title="Удалить"
                       class="btn btn-danger btn-xs"
                       onclick="return confirm('Вы уверены, что хоитте удалить этот элемент?')"
                    >
                        <i class="fa fa-trash-o"></i>
                    </a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
