<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;
use backend\modules\blog\models\Item;
use backend\modules\blog\models\Category;
use backend\modules\blog\models\Author;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\blog\models\ItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Посты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-index">
    <h1>
        <?= Html::encode($this->title); ?>
        <a href="<?= Url::to(['create']); ?>" class="btn btn-success block right">
            <i class="fa fa-plus" aria-hidden="true"></i>Добавить пост
        </a>
    </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width: 60px;']
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->title, ['view', 'id' => $model->id]);
                },
            ],
            [
                'attribute' => 'category_id',
                'filter' => ArrayHelper::map(Category::find()->joinWith('lang')->all(), 'id', 'title'),
                'format' => 'raw',
                'value' => function ($model) {
                    $category = $model->category;
                    return $category ? Html::a($category->title, ['category/view', 'id' => $category->id]) : '';
                }
            ],
            [
                'attribute' => 'author_id',
                'filter' => ArrayHelper::map(Author::find()->joinWith('lang')->all(), 'id', 'name'),
                'format' => 'raw',
                'value' => function ($model) {
                    $author = $model->author;
                    return $author ? Html::a($author->name, ['author/view', 'id' => $author->id]) : '';
                }
            ],
            [
                'attribute' => 'published_at',
                'format' => 'raw',
                'contentOptions' => ['style'=>'width: 160px;'],
                'value' => function ($model) {
                    return date('Y-m-d H:i', $model->published_at);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'published_at',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'contentOptions' => ['style'=>'width: 160px;'],
                'value' => function ($model) {
                    return date('Y-m-d H:i', $model->created_at);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'attribute' => 'status',
                'filter' => Item::getAllStatusProperties(),
                'value' => 'statusDetail'
            ],
            [
                'attribute' => 'actual',
                'filter' => Item::getAllActualProperties(),
                'value' => 'actualDetail'
            ],
            [
                'contentOptions' => ['style'=>'width: 110px;'],
                'format' => 'raw',
                'value' => function ($model) {
                    return
                        Html::a('<i class="fa fa-eye"></i>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-primary btn-xs', 'title' => 'Просмотреть']
                        ) . ' ' .
                        Html::a('<i class="fa fa-pencil"></i>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-info btn-xs', 'title' => 'Редактировать']
                        ) . ' ' .
                        Html::a('<i class="fa fa-trash-o"></i>',
                            ['delete', 'id' => $model->primaryKey],
                            [
                                'class' => 'btn btn-danger btn-xs',
                                'title' => 'Удалить',
                                'onclick' => 'return confirm("Вы уверены, что хотите удалить данную запись?")',
                                'data-method' => 'post'
                            ]
                        );
                }
            ],
        ],
    ]); ?>
</div>
