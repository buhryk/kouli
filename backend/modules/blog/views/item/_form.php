<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget as ImperaviWidget;
use yii\helpers\Url;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use backend\modules\blog\models\Category;
use backend\modules\blog\models\Item;
use backend\modules\blog\models\Author;
use mihaildev\elfinder\InputFile;
use kartik\select2\Select2;
use backend\modules\blog\models\Tag;

/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\Item */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelLang, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($modelLang, 'short_description')->textarea(['rows' => 3]) ?>
    <?= $form->field($modelLang, 'text')->widget(\backend\widgets\ImperaviWidget::className(), []) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-4 col-sm-12">
            <?= $form->field($model, 'preview_image')->widget(InputFile::className(), [
                'language'      => 'ru',
                'controller'    => 'elfinder',
                'filter'        => 'image',
                'path'          => 'common',
                'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false
            ])->label($model->getAttributeLabel('preview_image') ); ?>
            <?php if ($model->preview_image) {
                echo Html::img($model->preview_image, ['width' => 200]) . '<br><br>';
            } ?>
        </div>

    </div>

    <div class="row">
        <div class="col-md-3 col-sm-12">
            <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Category::find()->joinWith('lang')->all(), 'id', 'title')) ?>
        </div>
        <div class="col-md-3 col-sm-12">
            <?= $form->field($model, 'author_id')->dropDownList(ArrayHelper::map(Author::find()->joinWith('lang')->all(), 'id', 'name')) ?>
        </div>
        <div class="col-md-3 col-sm-12">
            <?= $form->field($model, 'status')->dropDownList(Item::getAllStatusProperties()) ?>
        </div>
        <div class="col-md-3 col-sm-12">
            <?= $form->field($model, 'actual')->dropDownList(Item::getAllActualProperties()) ?>
        </div>
        <div class="col-md-3 col-sm-12">
            <?= $form->field($model, 'published_at')->widget(DateTimePicker::classname(), [
                'options' => [],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd hh:ii'
                ]
            ]); ?>
        </div>
        <div class="col-md-3 col-sm-12">
            <?= $form->field($model, 'date')->widget(DateTimePicker::classname(), [
                'options' => [],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
