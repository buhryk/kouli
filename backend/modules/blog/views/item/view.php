<?php

use yii\helpers\Html;
use backend\modules\blog\models\Tag;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\Item */

$this->title = "Пост #$model->id";
$this->params['breadcrumbs'][] = ['label' => 'Посты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i>Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данную запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('short_description'); ?></th>
            <td><?= $model->short_description; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('category_id'); ?></th>
            <td>
                <?php $category = $model->category;
                echo $category ? Html::a($category->title, ['category/view', 'id' => $category->id]) : ''; ?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('author_id'); ?></th>
            <td>
                <?php $author = $model->author;
                echo $author ? Html::a($author->name, ['author/view', 'id' => $author->id]) : ''; ?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('alias'); ?></th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('preview_image'); ?></th>
            <td><?= $model->preview_image ? Html::img($model->preview_image, ['width' => 200]) : ''; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('status'); ?></th>
            <td><?= $model->statusDetail; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('actual'); ?></th>
            <td><?= $model->actualDetail; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('published_at'); ?></th>
            <td><?= date('Y-m-d H:i:s', $model->published_at); ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('date'); ?></th>
            <td><?= date('Y-m-d H:i:s', $model->date); ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('created_at'); ?></th>
            <td><?= date('Y-m-d H:i:s', $model->created_at); ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('updated_at'); ?></th>
            <td><?= date('Y-m-d H:i:s', $model->updated_at); ?></td>
        </tr>
        </tbody>
    </table>
</div>
