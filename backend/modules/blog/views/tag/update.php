<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\Tag */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Теги постов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Тег #$model->id", 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="tag-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>
</div>
