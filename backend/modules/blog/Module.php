<?php

namespace backend\modules\blog;


class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\blog\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}