<?php

namespace backend\modules\blog\controllers;

use backend\modules\blog\models\TagLang;
use backend\modules\transliterator\services\TransliteratorService;
use Yii;
use backend\modules\blog\models\Tag;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TagController implements the CRUD actions for Tag model.
 */
class TagController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \backend\modules\accesscontrol\AccessControlFilter::className(),
            ]
        ];
    }

    /**
     * Lists all Tag models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Tag::find(),
            'pagination' => false
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tag model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tag model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tag();
        $modelLang = new TagLang();

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            $modelLang->lang = Yii::$app->language;
            $modelLang->record_id = 0;

            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->record_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->primaryKey]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    /**
     * Updates an existing Tag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLang = $model->lang;
        if (!$modelLang) {
            $modelLang = new TagLang();
            $modelLang->lang = Yii::$app->language;
            $modelLang->record_id = $id;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    /**
     * Deletes an existing Tag model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tag model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tag::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
