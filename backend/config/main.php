<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'language' => 'uk-UK',
    'name' => 'Kouli',

    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'core' => [
            'class' => 'backend\modules\core\Module',
        ],
        'blog' => [
            'class' => 'backend\modules\blog\Module',
        ],
        'setting' => [
            'class' => 'backend\modules\setting\Module',
        ],
        'translate' => [
            'class' => 'backend\modules\translate\Module',
        ],
        'modules' => [
            'i18n' => Zelenin\yii\modules\I18n\Module::className()
        ],

    ],
    'controllerMap' => [
        'export' => 'phpnt\exportFile\controllers\ExportController',
        'elfinder' => [
            'class' => 'mihaildev\elfinder\PathController',
            'access' => ['@'],
            'root' => [
                'baseUrl' => '/uploads',
                'basePath' => '@frontend/web/uploads',
                //'path' => '/uploads/images/news/',
                'name' =>'Files'
            ],
        ],
    ],
    'components' => [
        'request' => [
            'baseUrl' => '/admin',
            'csrfParam' => '_csrf-backend',
            'class' => 'backend\components\LangRequest',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                    'sourceLanguage' => 'en',
                    'fileMap' => [
                        //'main' => 'main.php',
                    ],
                ],
            ],
        ],

        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
    'params' => $params,
];

//$config['modules']['gii'] = [
//    'class' => 'yii\gii\Module',
//    'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20'],
//    'generators' => [ //here
//        'crud' => [ // generator name
//            'class' => 'yii\gii\generators\crud\Generator', // generator class
//            'templates' => [ //setting for out templates
//                'myCrud' => '@common/templates/gii/crud/default', // template name => path to template
//            ]
//        ]
//    ],
//];
