(function($)
{
    $.Redactor.prototype.readyblocks = function()
    {
        return {

            init: function()
            {
                var that = this;
                var dropdown = {};
                var blocks_key = {};
                var blocks = [
                    {   title: 'Учасник',
                        key: 'users',
                        content: '<div class="person item-elemet">\n' +
                        '\t\t\t\t<img src="/images/composition/17.jpg" alt="person">\n' +
                        '\t\t\t\t<h4>Рудник-Іващенко Ольга Іванівна</h4>\n' +
                        '\t\t\t\t<p>заступник директора з науково-інноваційної роботи Інституту садівництва НААН, д.с.-г.н., с.н.с.\n' +
                        '\t\t\t\t</p>\n' +
                        '\t\t\t</div>'
                    },
                    {   title: 'Файл',
                        key: 'file',
                        content: '<div class="position-item item-elemet">\n' +
                        '\t\t\t\t\t<div class="position-item__image">\n' +
                        '\t\t\t\t\t\t<img src="/images/position.jpg" alt="position">\n' +
                        '\t\t\t\t\t</div>\n' +
                        '\t\t\t\t\t<div class="position-item__info">\n' +
                        '\t\t\t\t\t\t<p>Положення про вчену раду 2017</p>\n' +
                        '\t\t\t\t\t\t<div class="link-container">\n' +
                        '\t\t\t\t\t\t\tЗавантажити\n' +
                        '\t\t\t\t\t\t</div>\n' +
                        '\t\t\t\t\t</div>\n' +
                        '\t\t\t\t</div>'
                    },
                    {   title: 'Галерея',
                        key: 'gallery',
                        content:'\n<div class="photo-gallery__item item-elemet">\n'+
                       ' <img src="/images/photo-gallery.jpg" alt="photo-gallery">\n'+
                    '</div>\n'
                    },
                    {   title: 'Розрив блоків',
                        key: 'block-breaking',
                        content: '<div class="block-breaking" ></div>'
                    },
                ];
                var item;

                for (var key in blocks) {
                    var i = 0;
                    item = blocks[key];
                    blocks_key[item.key] = item;
                    dropdown[item.key] = {title: item.title, func: function(key) {
                        var current_item = blocks_key[key];
                        var current = $(this.selection.current());

                        if (current.parents('.item-elemet').hasClass('item-elemet')) {


                                current.parents('.item-elemet').after(current_item.content);
                                this.code.sync();


                        } else {
                            if(key == 'gallery'){
                                if(!current.parents('.photo-gallery').hasClass('photo-gallery') ) {
                                    this.insert.html('<div class="photo-gallery">' + current_item.content);
                                    i = 0;
                                }
                                i++;
                            }

                            if(key == 'users'){

                                if(!current.parents('.leadership-category__staff').hasClass('leadership-category__staff') ) {
                                    this.insert.html('<div class="leadership-category__staff">' + current_item.content);
                                    i = 0;
                                }
                                i++;
                            }

                            if(i > 1 || key == 'file') {
                                this.insert.html(current_item.content);
                                i--;
                            }


                        }
                        //
                    }};
                }
                console.log(dropdown);

                var button = this.button.add('readyblocks', 'Блоки');
                this.button.setIcon(button, '<i class="fa fa-address-card"></i>');
                this.button.addDropdown(button, dropdown);
            },
            set: function(content)
            {
                this.inline.html(content);
            },

        };
    };
})(jQuery);