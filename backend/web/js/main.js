/**
 * Created by Borys on 11.03.2017.
 */
$( document ).ready(function() {

    $('body').on('click', '.select-all', function () {
        var checkbox = $('.checkbox-item input[type = checkbox]');
        if ($(this).prop('checked') == true) {
            checkbox.prop('checked', true)
        } else {
            checkbox.prop('checked', false)
        }
    });
});