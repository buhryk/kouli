<?php
namespace backend\models;

use common\behaviors\PositionBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii;
use yii\helpers\ArrayHelper;

class BaseDataModel extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_DELETE = 2;

    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at', 'position'], 'integer'],
        ];
    }

    public function behaviors()
    {
        return [
            PositionBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'created_at' => 'Створено',
            'updated_at' => 'Редаговано',
        ];
    }


    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Активний'),
            self::STATUS_NOT_ACTIVE => Yii::t('app', 'Не активний'),
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusList()[$this->status]) ? self::getStatusList()[$this->status] : '';
    }


    /**
     * @param bool $deletedHide
     * @return object
     */
    public static function find($deletedHide = true)
    {
        $q = yii::createObject(yii\db\ActiveQuery::className(), [get_called_class()]);

        if ($deletedHide) {
            $q->andWhere(['!=', static::tableName() . '.status', self::STATUS_DELETE]);
        }

        return $q;
    }

    static public function getModelsAll($map = false)
    {
        $query = self::find()->where(['status' => self::STATUS_ACTIVE])->orderBy(['position' => SORT_ASC]);

        $models = $query->all();
        if ($map) {
            return ArrayHelper::map($models, 'id', 'name');
        }
        return $models;
    }
}