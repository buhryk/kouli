<?php

namespace backend\models;

use common\behaviors\LangBehavior;
use common\models\Lang;
use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "people".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property string $image
 * @property int $updated_at
 * @property int $created_at
 * @property int $status
 */
class People extends BaseDataModel
{
    /**
     * {@inheritdoc}
     */

    public $name;
    public $text;
    public $description;
//    public $record_id;
//    public $lang_id;

    public static function tableName()
    {
        return 'people';
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [

            [
                'class' => LangBehavior::className(),
                't' => new PeopleLang(),
                'fk' => 'record_id',
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'alias',
                'ensureUnique' => true
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
//            [['text', 'description'], 'string'],
            [['image', 'imagepng', 'alias'], 'string'],
            [['instagram'], 'string', 'max' => 200],
            [['status_on_main'], 'integer'],
            [[ 'name', 'text', 'description', 'record_id'], 'safe']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(),[
            'name' => Yii::t('app', 'Name'),
            'text' => Yii::t('app', 'Text'),
            'image' => Yii::t('app', 'Image'),
        ]);
    }

    public function getLang()
    {
        return $this->hasMany(PeopleLang::className(), ['record_id' => 'id'])->where(['lang_id' => Lang::getCurrent()->id]);
    }

}
