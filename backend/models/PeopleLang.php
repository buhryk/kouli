<?php

namespace backend\models;

use common\models\Lang;
use Yii;

/**
 * This is the model class for table "people_lang".
 *
 * @property int $record_id
 * @property int $lang_id
 * @property string $name
 * @property string $description
 * @property string $text
 *
 * @property Lang $lang
 * @property People $record
 */
class PeopleLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'people_lang';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(),[
            [['record_id', 'lang_id', 'name', 'text'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['text'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 555],
            [['record_id', 'lang_id'], 'unique', 'targetAttribute' => ['record_id', 'lang_id']],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => People::className(), 'targetAttribute' => ['record_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'record_id' => Yii::t('app', 'Record ID'),
            'lang_id' => Yii::t('app', 'Lang ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'text' => Yii::t('app', 'Text'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }
    public function getRecord()
    {
        return $this->hasOne(People::className(), ['id' => 'record_id']);
    }
}
