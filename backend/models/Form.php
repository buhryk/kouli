<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "forms".
 *
 * @property int $id
 * @property string $email
 * @property string $name
 * @property string $subject
 * @property string $message
 * @property int $updated_at
 * @property int $created_at
 * @property int $status
 * @property int $position
 */
class Form extends BaseDataModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'forms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(),[
            [['message'], 'string'],
            [['email'], 'string', 'max' => 50],
            [['name', 'subject'], 'string', 'max' => 250],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(),[
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'E-mail'),
            'name' => Yii::t('app', 'Имя'),
            'subject' => Yii::t('app', 'Телефон'),
            'message' => Yii::t('app', 'Сообщение'),
        ]);
    }
}
