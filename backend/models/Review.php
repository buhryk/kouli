<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "review".
 *
 * @property int $id
 * @property string $author
 * @property string $image
 * @property int $created_at
 * @property int $updated_at
 * @property string $text
 * @property int $status
 * @property int $position
 */
class Review extends BaseDataModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'review';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {

        return array_merge(parent::rules(),[
            [['author', 'text'], 'required'],
            [['id', 'created_at', 'updated_at', 'status', 'position'], 'integer'],
            [['text'], 'string'],
            [['author', 'image'], 'string', 'max' => 255],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(),[
            'id' => Yii::t('app', 'ID'),
            'author' => Yii::t('app', 'Имя'),
            'image' => Yii::t('app', 'Изображение'),
            'text' => Yii::t('app', 'Сообщение'),
        ]);
    }
}
