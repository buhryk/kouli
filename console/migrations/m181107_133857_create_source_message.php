<?php

use yii\db\Migration;

/**
 * Class m181107_133857_create_source_message
 */
class m181107_133857_create_source_message extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('source_message', [
            'id' => $this->primaryKey(),
            'category' => $this->string(111),
            'message' => $this->string(555),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181107_133857_create_source_message cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181107_133857_create_source_message cannot be reverted.\n";

        return false;
    }
    */
}
