<?php

use yii\db\Migration;

/**
 * Class m180405_121632_add_table
 */
class m180405_121632_add_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('people', [
            'id' => $this->primaryKey(),
            'name' => $this->string('50'),
            'text' => $this->text()->null(),
            'image' => $this->string('50'),
            'updated_at' => $this->integer()->defaultValue(NULL),
            'created_at' => $this->integer()->defaultValue(NULL),
            'status' => $this->smallInteger()->defaultValue(1),
        ]);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180405_121632_add_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180405_121632_add_table cannot be reverted.\n";

        return false;
    }
    */
}
