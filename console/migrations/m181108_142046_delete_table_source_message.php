<?php

use yii\db\Migration;

/**
 * Class m181108_142046_delete_table_source_message
 */
class m181108_142046_delete_table_source_message extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('source_message');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181108_142046_delete_table_source_message cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181108_142046_delete_table_source_message cannot be reverted.\n";

        return false;
    }
    */
}
