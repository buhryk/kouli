<?php

use yii\db\Migration;

/**
 * Class m191017_114254_add_tables
 */
class m191017_114254_add_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public $lang = '{{%lang}}';

    public $page = '{{%page}}';
    public $page_lang = '{{%page_lang}}';

    public $category = '{{%page_category}}';
    public $category_lang = '{{%page_categor_lang}}';

    public $widget = '{{%widget}}';
    public $widget_lang = '{{%widget_lang}}';

    public $setting = '{{%setting}}';

    public function safeUp()
    {

        $this->createTable($this->page, [
            'id' => $this->primaryKey(),
            'position' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'image' => $this->string(),
            'alias' => $this->string(),
            'category_id' => $this->integer(),
            'additional_data' => $this->json(),
        ]);

        $this->createTable($this->page_lang, [
            'record_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'title' => $this->string(128)->notNull(),
            'description' => $this->text()->null(),
            'text' => $this->text()->notNull(),
        ]);


        $this->createTable($this->widget, [
            'id' => $this->primaryKey(),
            'position' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'key' => $this->string()->unique(),
            'title' => $this->string()->notNull(),
            'additional_data' => $this->json(),
        ]);

        $this->createTable($this->widget_lang, [
            'record_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'content' => $this->text(),
        ]);

        $this->createTable($this->setting, [
            'id' => $this->primaryKey(),
            'key' => $this->string()->unique(),
            'group' => $this->string()->notNull(),
            'value' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'additional_data' => $this->json(),
        ]);

        $this->addForeignKey('page-lang-id', $this->page_lang, 'lang_id', $this->lang, 'id', 'CASCADE');
        $this->addForeignKey('page-record-id', $this->page_lang, 'record_id', $this->page, 'id', 'CASCADE');

        $this->addForeignKey('widget-lang-id', $this->widget_lang, 'lang_id', $this->lang, 'id', 'CASCADE');
        $this->addForeignKey('widget-record-id', $this->widget_lang, 'record_id', $this->widget, 'id', 'CASCADE');

        $this->addPrimaryKey('page_lang-pk', $this->page_lang, ['record_id', 'lang_id']);
        $this->addPrimaryKey('widget_lang-pk', $this->widget_lang, ['record_id', 'lang_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191017_114254_add_tables cannot be reverted.\n";

        return false;
    }

    public function json()
    {
        return $this->getDb()->getSchema()->createColumnSchemaBuilder('JSON');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191017_114254_add_tables cannot be reverted.\n";

        return false;
    }
    */
}
