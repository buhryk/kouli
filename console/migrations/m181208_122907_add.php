<?php

use yii\db\Migration;

/**
 * Class m181208_122907_add
 */
class m181208_122907_add extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('people', 'alias', $this->string(255));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181208_122907_add cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181208_122907_add cannot be reverted.\n";

        return false;
    }
    */
}
