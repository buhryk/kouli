<?php

use yii\db\Migration;

/**
 * Class m181105_124524_add_position_to_people
 */
class m181105_124524_add_position_to_people extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('people', 'position', $this->smallInteger()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181105_124524_add_position_to_people cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181105_124524_add_position_to_people cannot be reverted.\n";

        return false;
    }
    */
}
