<?php

use yii\db\Migration;

/**
 * Class m181208_181122_add_forms_table
 */
class m181208_181122_add_forms_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('forms', [
            'id' => $this->primaryKey(),
            'email' => $this->string('50'),
            'name' => $this->string('250'),
            'subject' => $this->string('250'),
            'message' => $this->text(),
            'updated_at' => $this->integer()->defaultValue(NULL),
            'created_at' => $this->integer()->defaultValue(NULL),
            'status' => $this->smallInteger()->defaultValue(1),
            'position' => $this->integer()->defaultValue(1),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181208_181122_add_forms_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181208_181122_add_forms_table cannot be reverted.\n";

        return false;
    }
    */
}
