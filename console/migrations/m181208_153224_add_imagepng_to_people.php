<?php

use yii\db\Migration;

/**
 * Class m181208_153224_add_imagepng_to_people
 */
class m181208_153224_add_imagepng_to_people extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('people', 'imagepng', $this->string(155));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181208_153224_add_imagepng_to_people cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181208_153224_add_imagepng_to_people cannot be reverted.\n";

        return false;
    }
    */
}
