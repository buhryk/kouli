<?php

use yii\db\Migration;

/**
 * Class m181109_101956_drop_columns_from_people
 */
class m181109_101956_drop_columns_from_people extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('people', 'name');
        $this->dropColumn('people', 'text');
        $this->dropColumn('people', 'description');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181109_101956_drop_columns_from_people cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181109_101956_drop_columns_from_people cannot be reverted.\n";

        return false;
    }
    */
}
