<?php

use yii\db\Migration;

/**
 * Class m180419_161216_add_user
 */
class m180419_161216_add_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $model = new \frontend\models\SignupForm();
        $model->email = 'admin@admin.ru';
        $model->username = 'admin';
        $model->password = 'qwerty123';
        $model->signup();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180419_161216_add_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180419_161216_add_user cannot be reverted.\n";

        return false;
    }
    */
}
