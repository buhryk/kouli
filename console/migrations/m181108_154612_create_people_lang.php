<?php

use yii\db\Migration;

/**
 * Class m181108_154612_create_people_lang
 */
class m181108_154612_create_people_lang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('people_lang', [
            'record_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'description' => $this->string(555)->null(),
            'text' => $this->text()->notNull(),
        ]);

        $this->addForeignKey('people-lang-id', 'people_lang', 'lang_id', 'lang', 'id', 'CASCADE');
        $this->addForeignKey('people-record-id', 'people_lang', 'record_id', 'people', 'id', 'CASCADE');

        $this->addPrimaryKey('people-pk', 'people_lang', ['record_id', 'lang_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181108_154612_create_people_lang cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181108_154612_create_people_lang cannot be reverted.\n";

        return false;
    }
    */
}
