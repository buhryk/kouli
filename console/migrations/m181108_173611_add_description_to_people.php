<?php

use yii\db\Migration;

/**
 * Class m181108_173611_add_description_to_people
 */
class m181108_173611_add_description_to_people extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('people', 'description', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181108_173611_add_description_to_people cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181108_173611_add_description_to_people cannot be reverted.\n";

        return false;
    }
    */
}
