<?php

use yii\db\Migration;

/**
 * Class m181107_100110_add_new_columns_to_lang
 */
class m181107_100110_add_new_columns_to_lang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lang', 'status', $this->smallInteger()->defaultValue(1));
        $this->addColumn('lang', 'position', $this->smallInteger()->defaultValue(1));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181107_100110_add_new_columns_to_lang cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181107_100110_add_new_columns_to_lang cannot be reverted.\n";

        return false;
    }
    */
}
