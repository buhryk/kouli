<?php

use yii\db\Migration;

/**
 * Class m181108_174128_add_status_on_the_main_to_people
 */
class m181108_174128_add_status_on_the_main_to_people extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('people', 'status_on_main', $this->smallInteger()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181108_174128_add_status_on_the_main_to_people cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181108_174128_add_status_on_the_main_to_people cannot be reverted.\n";

        return false;
    }
    */
}
