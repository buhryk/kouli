<?php

use yii\db\Migration;

/**
 * Class m181105_174837_add_instagram_to_people
 */
class m181105_174837_add_instagram_to_people extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('people', 'instagram', $this->string(200)->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181105_174837_add_instagram_to_people cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181105_174837_add_instagram_to_people cannot be reverted.\n";

        return false;
    }
    */
}
